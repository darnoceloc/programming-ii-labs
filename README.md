# Programming II (Introduction to Object-Oriented Programming) Labs

This repository holds the labs material for Programming II subject in 2017-2018 study year.

---

## Week 1

### Recap on procedural programming (Programming I)

1. Primitive data types: representation, domain, operations
2. Variables and constants. Static local variables
3. Functions: declaration, definition, call stack
4. Structures
5. Pointers

### Test-driven development

1. Write the tests first
2. Write the code so that the tests pass

### Several Non-OOP additions in C++

**bool data type**

1. May hold two values: true or false
2. sizeof(bool) is implementation defined; may be different from 1

**Default function arguments***

1. applicable to any function
2. cannot redeclare a default in the same scope
3. local variable cannot be a default
4. after a parameter with a default argument, all subsequent parameters must either
* have a default argument supplied in this or a previous declaration; or
* be a function parameter pack (since C++11)
5. for member functions of class templates, all defaults must be provided in the initial declaration of the member function
6. operator functions shall not have default arguments, except for the function call operator.

Example 1
```C++
	int f(int n = 0, int); 
	int g(int n=0, ...);
```

Example 2
```C++
	void f(int, int);     // #1 
	void f(int, int = 7); // #2 OK: adds a default
	void f(int = 1, int); // #3 OK, adds a default to #2

	void n() { 
		f();     // calls f(1, 7);
		f(2, 3); // calls f(2, 3);
		f(10);   // calls f(10, 7);
	}
```

Example 3
```C++
	int a = 1;
	int f(int);
	int g(int x = f(a)); // lookup for f finds ::f, lookup for a finds ::a
						 // the value of ::a, which is 1 at this point, is not used
	void h()
	{
	  a = 2;  // changes the value of ::a
	  {
		 int a = 3;
		 g();       // calls f(2), then calls g() with the result
	  }
	}
```

---

## Week 2

### I/O Streams

1. Output stream: cout
2. Input stream: cin
3. Error stream: cerr
4. Left/right shift operators (<</>>) to put/get data to/from streams


### inline functions

1. The original intent of the `inline` keyword was to serve as an indicator to the optimizer that inline substitution of a function is preferred over function call, that is, instead of executing the function call CPU instruction to transfer control to the function body, a copy of the function body is executed without generating the call. This avoids overhead created by the function call (copying the arguments and retrieving the result) but it may result in a larger executable as the code for the function has to be repeated multiple times.
2. Since this meaning of the keyword inline is *non-binding*, compilers are free to use inline substitution for any function that's not marked inline, and are free to generate function calls to any function marked inline. 
3. A function defined entirely inside a class/struct/union definition, whether it's a member function or a non-member friend function, is implicitly an inline function
4. A function declared `constexpr` is implicitly an inline function


### Function name overloading

1. Two or more functions with the same name
2. List or arguments must be different

### References
[1. Default arguments](http://en.cppreference.com/w/cpp/language/default_arguments)

[2 Inline functions](http://en.cppreference.com/w/cpp/language/inline)

## Exercise
## Write the complex class such that the [Lab2 test program](src/Lab2.cpp) passes successfully.

---

## Week 3. Constructors

### Short summary of constructors:

1. purpose
2. declaration, definition
3. default constructor
4. copy constructor
5. type-conversion constructor
6. explicit, default, delete

### Exercise 1

1. Start off with Lab 2’s complex class and add a custom constructor  `complex(int, int)`
2. Note that the declaration `complex x` is erroneous; explain why
3. Fix this error by providing defaults for constructor’s arguments 

### Exercise 2

Write the code to pass the following test:

```C++
	void Lab2Tests::testComplex3() {
    	complex c1, c2{2}, c3{3,4}, c4{0, 1}, c5=c2;

    	assert(c1.real()==0 && c1.imag()==0, "c1 mis-initialized.");
    	// similar for c2 - c5
	}
```
	
### Exercise 3

1. Add copy constructor to class complex
2. Note that the argument can be modified if not marked as constant
3. Add `const` modifier to prevent unintended modifications

### Exercise 4 

1. Create and call the following function
	`void f(complex c) { 
	}`
2. Note that copy constructor is called; explain why
3. Avoid creating unnecessary objects on the stack using reference arguments
4. Caution: when passed by reference, modification made inside f() are performed directly on the object itself and are persistent beyond function’s call.

## Homework
## Write the `str` class such that the [Lab3 test program](src/Lab3.cpp) passes successfully.

---

## Week 4. Constructors (continued). Memory allocation. constness

### Resource management

1. what is a resource
2. resource allocation / de-allocation (release)
3. memory allocation/free using `new` and `delete` operators


### Exercise 1

1. start-off with class `str` from previous lab homework
2. check that memory allocation is handled correctly in ctor, dtor and copy-ctor
3. transform the custom constructor into an `explicit` one and explain behavior

### Exercise 2:

1. const non-static member functions. Try to add `std::cout << r.rep();` in the copy constructors of class str. 
2. Note the error, because `r` is constant reference
3. Fix the error by turning rep() non-static member function into a const non-static member function.
4. `const` is part of function’s signature, thus it needs to appear in declaration and implementation as well; as a side note, we may have 2 functions with exact same prototype, except for const keyword (e.g. `char* rep()` and `char* rep() const` are 2 different functions); compiler uses the lvalue to pick the one that’s invoked
5. Calling non-const non-static member functions on const references is flagged as erroneous

## Homework
## Write the `str` class such that the [Lab4 test program](src/Lab4hw.cpp) passes successfully.

---

## Week 5. Static and non-static members.

### static data member
1. explain what they are; 
2. how are initialized
3. how are accessed

### static member functions
1. explain what they are
2. can’t access non-static members


### Exercise 1

1. Add `countInstances` static member function to `str` type such that the following test passes

```C++
    void testCounter() {
        str s1="This is test", s2, s3=s1;
        assert(3==str::countInstances(), "Number of instances mismatch.");
    }
```

### non-static objects (data members whose type is another class, not a primitive C/C++ type)

1. A class may have data members whose type is another class; for example the name of a Student is represented as str, while Date is used to represent the date of birth
2. To avoid their initialization by the default ctor followed by an assignment (=), these members are better initialized in ctor's initialization list

### Exercise 2
3. Add `cout` traces in str and Date constructors (custom and default)
4. Run the first implementation of Studnet's ctor below and check what ctors are invoked
5. Run the second implementation of Studnet's ctor below and check what ctors are invoked

```C++
    class Student {
        Date dob; // non-static member
        str name; // non-static member
    public:
        Student(const char* n, const Date& d);
    };
	
    // *NOT OK* as it will first initialize dob and name using the defautl ctor.
    Student::Student(const char* n, const Date& d) {
        dob = d; // assignment (change) , *NOT* initialization
        name = String{n}; // assignment (change) , *NOT* initialization
    }

    // This is better as it initializes dob and name with proper ctors.
    Student::Student(const char* n, const Date& d) 
        : dob{d}, name{n} {
    }
```

## Homework
## Write the `Student` class such that the [Lab5 test program](src/Lab5hw.cpp) passes successfully. Do reuse the str class created in the previous homework to represent student's name. Make sure you use initialization list in constructors' implementation. The implementation of class Date bundled within Lab5.cpp should not be modified as it should suffices the requirements in this test.

### Please note that from submitted homeworks I only pick-up classes of interest and paste the code into the testing skeleton. For this homework, I will only use class Student and its implementation. So, make sure you don't modify tests or Date implementation to fit your code, rather otherwise.


---

## Week 6. Orthodox Good Friday

---


## Week 7. Operator overloading

1. complete data types should support operations (operator based) on their instances
2. not supported by all object oriented languages
3. constraints on operator overloading
4. operator function as member function
5. operator function as non-member function
     it enables to compute expressions such as c+5 and 5+c
6. discuss the return value

### Exercise 1 

1. Consider class `complex` from Lab3, add operators `+=, +, ==`

```C++
class complex {

	complex operator += (const complex& r);

	//....
};

complex operator+(const complex& left, const complex& right);

bool operator==(const complex& left, const complex& right);

```

(See the complete solution [here](src/Lab7s.cpp))

### Additional remarks

1. a+=b <=> a.operator+=(b);
2. c+5 <=> operator+(c, complex(5));
3. assignment (=); always as member function
4. arithmetic: those that change the internal representation of lhs (+= and similar), those that didn’t (+ and similar)
5. comparison: == and < (all others expressed based on these two)
6. Bitwise left/right shift (<<. >>): used to put/get user-defined objects to streams
7. Type conversion operator: operator int(); doesn’t declare a return value

### References

[1.] (http://en.cppreference.com/w/cpp/language/operators)

[2.] (https://en.wikibooks.org/wiki/C%2B%2B_Programming/Operators/Operator_Overloading)


## Homework
## Implement the classes `List` and `List::Iterator` so the [Lab7 test program](src/Lab7hw.cpp) passes successfully. Due to a higher complexity, this homework is awarded double number of points (max 200).

---

## Week 8. Inheritance

1. Inheritance. Definition. Properties
2. Access control
3. Pointers to base class. Initialization. Objects handling through pointers/references to base class. Pointers to private base classes
4. Up-casting, down-casting

### Exercise 1

1. 'Translate' the following class diagram in C++ code.
Legend:
* `-` means `private`
* `#` means `protected`
* `+` means `public`

![Shape class diagram](http://yuml.me/05ce74d2.png "Shape class diagram")

(See the complete solution [here](src/Lab8s.cpp))

### Exercise 2
1. Consider the classes `Shape, Point` and `Circle` implemented in Exercise 1. Analyze which cast operations are valid in the following code

```C++
int main() {
    Shape* ps;
    //ps = new Point; // error <- private base
    ps = new Circle; // okay
	//ps = new Line; // error <- protected base
    Point* pp = new Point; // okay
    
	//Circle *pc = new Shape; // error <- down-casting
    Circle* pc = static_cast<Circle*> (new Shape); // forced down-casting
	
	return 0;
}

```

---

## Week 9. Inheritance (continued)


1. Constructors and destructor; order of invocation
2. Usage of initialization list

### Exercise 1

1. Add the default and copy constructors to `Shape, Point, Circle` and `Line` classes from previous week
2. Add traces in ctors and dtor (e.g. `std::cout << "Shape::Shape" << std::endl;`)
3. Observe the order of invocation in the following code

```C++
void testCtors() {
	Point p;
	Circle c;
	Line l;
	Shape* ps = new Point(1, 2);
	delete ps;
}
```

3. Function overriding 
4. Accessing members of base class (`Shape::draw()`)
5. Polymorphism: getting the correct behavior through pointer access `pm->print()`


### Exercise 2

1. Create an empty class `Context`
2. Add `void draw(const Context& ctx)` member function in Shape and derived classes
3. Adjust all inheritance access control to public, hence Shape became a public base class for Line and Point (i.e. class Line : public Shape { ...)
4. Observe what functions are invoked in the below code when `virtual` keyword is/isn't use in base class (Shape) 

```C++
void testPolymorphism() {
    Context context;
   	std::list<Shape*> shapes;
	Point p;
	Circle c;
	Line l;
	Shape* ps = new Point(1, 2);
	
	shapes.push_back(&p);
	shapes.push_back(&c);
	shapes.push_back(&l);
	shapes.push_back(ps);
	
	for(list<Shape*>::iterator i = shapes.begin(); i != shapes.end(); i++) {
	    (*i)->draw(context);
	}

	delete ps;
}
```

See the solution [here](src/Lab9s.cpp).

## Homework
## Music world is beautiful and diverse. [Lab9 test program](src/Lab9hw.cpp) is a mere starting point in modelling it. Implement classes MusicPerformer, Singer and Band such that all tests pass.

---


## Week 10. Exception handling

### Short summary of exception handling
1. Exception  - a runtime error
2. Stages, which usually happens in different software modules/components
* Detection and signaling
* Handling 
3. `throw` / `try` / `catch` mechanism - short explanation

### Exercise 1. Basics of exception handling

Given the code in [Lab10 program](src/Lab10.cpp).

1. Implement `File::write` method
2. Add a common base class, `IOException`, for both `FileNotFoundException` and `FileWriteException `
3. Check the importance of the order of `catch` branches
4. Experiment the usage of reference (`IOException&` over `IOException`); we get a reference for the real exception object vs. gettig a (possibly sliced) copy, thus losing the initial type of the exception; verify this making `IOException` a polymorphic type with `describe` being the virtual method
5. Add a 'default' branch to handle any other exception; for this, (i) Add a new exception class (not inheriting from `IOException`), (ii) Add a `catch(...)` branch at the bottom most position.

### Exercise 2. Exceptions in constructors

The following example illustrates how exceptions can be used to notify callers on errors occured during object initialization.

```C++
#include <iostream>

class Vector {
 	static const int MAX_SIZE = 1000; // note: static members initalized on declaration
public:
 	class BadSize { } ; // a nested class to represent an exception
	
 	Vector(int sz) {
      	if(sz<0 || sz>MAX_SIZE) throw BadSize{};
      	// do the actual job
 	}
};

void f(int size) {
 	try {
      	Vector v(size);
      	std::cout << "Moving on..." << std::endl;
 	}
 	catch(Vector::BadSize& bs) {
      	std::cerr << "ERROR: Invalid vector size " << size << std::endl;
 	}
 	std::cout << "Return" << std::endl;
}

int main() {
 	f(-10); // Exception is handled
 	f(5); // OK
 	return 0;
}

```

## Homework
## [Lab10 test program](src/Lab10hw.cpp) should pass successfully.


---

## Week 11. Templates

### Short summary of templates and generic programming
1. What is a template (parameterized) class / function
2. Stages: code generation
3. C++ syntax

### Exercise 1. Basics of class templates

Given the `complex` in [Lab3](src/Lab3s.cpp), transform it to a template class, where the type of real and imaginary parts of a complex number is a parameter of the class. All tests should pass.

Remarks:

1. the template implicitly assumes some constraints on the type T, i.e. T is substituable for any data type that satisfies all of the following:

* can be initialized with an integer value (because of the default value in complex ctor) 
* has `operator<<` overloaded (for various `cout <<` traces in ctor)
* has `operator+` overloaded
* has `operator=` overloaded (assignment in setReal member)

Experiment with data types that meet all these constraints (e.g. int, float, void*) and with data types that don't (e.g. `class X{};`, void, Date etc.)

2. typedef frequent instatiations (e.g. `typedef complex<int> complexi;`)


See the solution [here](src/Lab11s.cpp).

## Homework
## [Lab11 test program](src/Lab11hw.cpp) should pass successfully.


---


## Week 12. Standard Template Library (STL)

### Brief overview over STL

1. Organization
2. Containers. Allocators. Iterators
3. Algorithms. Function objects (functors)
4. Numerics

### Exercise 1. Summing-up elements of a vector using iterator

```C++
void testWithIterator() {
    int values[] = {10, 20, 30, 40, 50};
    list<int> l{values, values+5};

    int sum = 0;
    for(list<int>::iterator i=l.begin(); i!=l.end(); i++) {
        sum += *i;
    }

    assert(150==sum, "Error computing sum");
}
```

An alternative solution,

```C++
template<class T> typename C::value_type sum(const C& container) {
     typename C::value_type s = 0;
     typename C::const_iterator p = container.begin();
     while(p!=container.end()) {
          s += *p;
          p++;
     }
     return s;
};

void testWithIterator() {
    int values[] = {10, 20, 30, 40, 50};
    list<int> l{values, values+5};
 
    assert(150==sum(l), "Error computing sum");
}

```

### Exercise 2. Summing-up elements of a vector using a function object (functor)

```C++
template<class T> class Summer {
    T sum = 0;
    
    public:
        void operator() (T x) {
            sum += x;            
        }
        
        operator T() {            
            return sum;
        }        
};

void testWithFunctor() {
    int values[] = {10, 20, 30, 40, 50};
    list<int> l{values, values+5};

    Summer<int> sum = for_each(l.begin(), l.end(), Summer<int>{});

    assert(150==sum, "Error computing sum");
}
```

### Exercise 3. Summing-up elements of a vector using accumulator algorithm

```C++
void testWithAccumulate() {
    int values[] = {10, 20, 30, 40, 50};
    list<int> l{values, values+5};

    assert(150==accumulate(l.begin(), l.end(), 0), "Error computing sum");
}
```


### Exercise 4. Summing-up elements of a vector using a lambda function

```C++
void testWithLambdaFunction() {
    int values[] = {10, 20, 30, 40, 50};
    list<int> l{values, values+5};
    auto lambdaExpr = [](int n) -> int {
		static int sum = 0; 
		sum +=n; 
		return sum;
	};
    for_each(l.begin(), l.end(), lambdaExpr);
    
    assert(150==lambdaExpr(0), "Error computing sum");
}
```

Download complete code from [here](src/Lab12s.cpp).

---

## Week 13. June 1st

---

## Week 14. A final example

The following class diagram illustrates a possible classificatio of university members.

![Uni staff class diagram](http://yuml.me/c4051f84.png "Uni staff class diagram")

* `IMyObject` is an interface, hence toStirng and clone are pure virtual member functions
* `UniMember` is an abstract class as it only overrides toString
* `Student` and `Teacher` are concrete classes
* `PhDStudent` is an example of multiple inheritance
* In `clone` override make sure you use covariant return types, i.e. returns `Student*` and `Teacher*`, when overriding it in Student and Teacher classes respectively
* Use virtual inheritance to avoid ambiguities in PhDStudent
* Do observe the usage of `dynamic_cast` operator instead of C-style casting , e.g. `(Student *)studentClone`; this is required as compiler cannot compute the pointer type at compile time due to the usage of virtual inheritance

The code below displays the tests need to be passed

```C++
void testNotInstantiable() {
    //UniMember uni{"Jo Doe", Date{1,1,1900}}; // => compilaiton error
}

void testStudent() {
    UniMember* student = new Student{"Popescu", Date{21, 3, 2000}};
    IMyObject* studentClone = student->clone();

    assert(0<strstr(typeid(*studentClone).name(), "Student"), "Wrong pointer type");
    assert(0==strcmp("Popescu", dynamic_cast<Student *>(studentClone)->name()), "Clone failed");
}

void testTeacher() {
    UniMember* teacher = new Teacher{"Popescu", Date{10, 10, 1977}};
    IMyObject* teacherClone = teacher->clone();

    assert(0<strstr(typeid(*teacherClone).name(), "Teacher"), "Wrong pointer type");
    assert(0==strcmp("Popescu", dynamic_cast<Teacher *>(teacherClone)->name()), "Clone failed");
}

void testPhDStudent() {
    UniMember* phd = new PhDStudent{"Popescu", Date{30, 8, 1992}};
    IMyObject* phdClone = phd->clone();

    assert(0<strstr(typeid(*phdClone).name(), "PhDStudent"), "Wrong pointer type");
    assert(0==strcmp("Popescu", dynamic_cast<PhDStudent *>(phdClone)->name()), "Clone failed");
}

```

Download complete code from [here](src/Lab14s.cpp).

---