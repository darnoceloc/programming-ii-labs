#include <iostream>
#include <list>
#include <algorithm>
#include <numeric>

using namespace std;

void assert(bool e, const char* m) {
    if (!e) std::cerr << "Assertion failed: " << m << std::endl;
}

void testWithIterator1() {
    int values[] = {10, 20, 30, 40, 50};
    list<int> l{values, values+5};

    int sum = 0;
    for(list<int>::iterator i=l.begin(); i!=l.end(); i++) {
        sum += *i;
    }

    assert(150==sum, "Error computing sum");
}


template<class C> typename C::value_type sum(const C& container) {
     typename C::value_type s = 0;
     typename C::const_iterator p = container.begin();
     while(p!=container.end()) {
          s += *p;
          p++;
     }
     return s;
};

void testWithIterator2() {
    int values[] = {10, 20, 30, 40, 50};
    list<int> l{values, values+5};
 
    assert(150==sum(l), "Error computing sum");
}


template<class T> class Summer {
    T sum = 0;
    
    public:
        void operator() (T x) {
            sum += x;            
        }
        
        operator T() {            
            return sum;
        }        
};

void testWithFunctor() {
    int values[] = {10, 20, 30, 40, 50};
    list<int> l{values, values+5};

    Summer<int> sum = for_each(l.begin(), l.end(), Summer<int>{});

    assert(150==sum, "Error computing sum");
}

void testWithAccumulate() {
    int values[] = {10, 20, 30, 40, 50};
    list<int> l{values, values+5};

    assert(150==accumulate(l.begin(), l.end(), 0), "Error computing sum");
}

void testWithLambdaFunction() {
    int values[] = {10, 20, 30, 40, 50};
    list<int> l{values, values+5};
    auto lambdaExpr = [](int n) -> int {
		static int sum = 0; 
		sum +=n; 
		return sum;
	};
    for_each(l.begin(), l.end(), lambdaExpr);
    
    assert(150==lambdaExpr(0), "Error computing sum");
}

int main() {
    testWithIterator1();
    testWithIterator2();
    testWithFunctor();
    testWithLambdaFunction();
    testWithAccumulate();
    return 0;
}
