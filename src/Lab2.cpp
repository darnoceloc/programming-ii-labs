// Lab2 program
#include <iostream>

class TestBase {        
    protected:
        void assert(bool expression, const char* message);
        
    public:
        virtual void runAllTests();
};


void TestBase::assert(bool expression, const char* message) {
    if (!expression) {
        std::cout << "Assertion failed: " << message << std::endl;
    }
}

void TestBase::runAllTests() {
}


class Lab2Tests : public TestBase {

    void testComplexClass1();
    
    void testComplexClass2();
    
    void testComplexClass3();    
    
    public:
        void runAllTests() override;
};

void Lab2Tests::testComplexClass1() {
    complex c;
    c.init(1, 2);
 
    assert(c.real()==1, "Real part mismatch");
    assert(c.imag()==2, "Imag part mismatch");
}

void Lab2Tests::testComplexClass2() {
    complex c1;
    c1.init(1, 2);
 
    complex c2;
    c2.init(2, 3);

    assert(c1.add(c2).real()==3, "Addition: real part mismatch");
    assert(c2.add(c1).imag()==5, "Addition: imag part mismatch");
}

void Lab2Tests::testComplexClass3() {
    complex c;
    c.init(1, 2);

    assert(c.add(2).real()==3, "Scalar addition: real part mismatch");
    assert(c.add(2).imag()==2, "Scalar addition: imag part mismatch");
}

void Lab2Tests::runAllTests() {
    testComplexClass1();
    testComplexClass2();
    testComplexClass3();
    std::cout << "All good";
}
    
int main() {
    Lab2Tests l2tests;
    l2tests.runAllTests();
    return 0;
}


