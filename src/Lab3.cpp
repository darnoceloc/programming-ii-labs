// Lab3 program
#include <iostream>

class TestBase {        
    protected:
        void assert(bool expression, const char* message);
        
    public:
        virtual void runAllTests();
};


void TestBase::assert(bool expression, const char* message) {
    if (!expression) {
        std::cout << "Assertion failed: " << message << std::endl;
    }
}

void TestBase::runAllTests() {
}

class Lab3Tests : public TestBase {
    void testStringClass1();
    
    public:
        void runAllTests() override;
};

void Lab3Tests::testStringClass1() {
    str s1 = "This is my string", s2, s3=s1; 

    assert(0==strcmp("This is my string", s1.rep()), "s1 representation error.");
    assert(NULL==s2.rep(), "s2 representation error.");
    assert(0==strcmp("This is my string", s3.rep()), "s3 representation error.");

    std::cout << s1.rep();
    std::cout << s3.rep();
}

void Lab3Tests::runAllTests() {
    testStringClass1();
    std::cout << "All good";
}

int main() {
    Lab3Tests l3tests;
    l3tests.runAllTests();
    return 0;
}
