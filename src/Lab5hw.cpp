#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>
#include <sstream>
#include <string.h>


// FreeStoreProxy.h
class FreeStoreProxy {
public: 
    static FreeStoreProxy& instance();
    
    void* allocate(size_t size);
    
    void release(void* ptr) ;
    
    int getDanglingPointersCount() const;
    
    void freeAll();

private:
    static const int CAPACITY = 1024;
    static FreeStoreProxy _instance;
    int dbgLevel;
    void* entries[CAPACITY];
    
    FreeStoreProxy(int debugLevel=0);
    void* const* position() const;
    void debug(const char* msg) const;
} ;

// FreeStoreProxy.cpp
FreeStoreProxy FreeStoreProxy::_instance;

inline void* const* FreeStoreProxy::position() const {
    return std::find(entries, entries+CAPACITY, nullptr);
}
    
FreeStoreProxy::FreeStoreProxy(int debugLevel) : dbgLevel{debugLevel} {
    std::fill_n(entries, CAPACITY, nullptr); 
    debug("FreeStoreProxy::FreeStoreProxy");
}

inline FreeStoreProxy& FreeStoreProxy::instance() {
    return _instance;
}   

void* FreeStoreProxy::allocate(size_t size) {
    void** pos = (void **)position();
    if (pos!=entries+CAPACITY) {
        void* ptr = malloc(size);
        *pos = ptr;
        debug("allocate memory");
        return ptr;
    }
    return nullptr;
}

void FreeStoreProxy::release(void* ptr) {
    for(int i=0; i<CAPACITY; i++) {
        if (entries[i]==ptr) {
            debug("free memory");
            free(ptr);
            entries[i] = nullptr;
            break;
        }
    }
}

int FreeStoreProxy::getDanglingPointersCount() const {
    // int counter = 0;
    // std::for_each(entries, entries+CAPACITY, [&](void* p) { counter += (p!=nullptr); });
    // return counter;
    //return std::accumulate(entries, entries+CAPACITY, 0, [](int a, void* p) { return a + (p!=nullptr); });
    return CAPACITY - std::count(entries, entries+CAPACITY, nullptr);
}

void FreeStoreProxy::freeAll() {
    for(int i=0; i<CAPACITY; i++) {
        if (entries[i]!=nullptr) {
            debug("free memory");
            free(entries[i]);
            entries[i] = nullptr;
        }
    }
}

void FreeStoreProxy::debug(const char* msg) const {
    if (dbgLevel>0) {
        std::cout << "FreeStoreProxy - " << msg << std::endl;
    }
}
        
void * operator new(size_t size) {
    return FreeStoreProxy::instance().allocate(size);
}

void operator delete(void *ptr) noexcept {
    FreeStoreProxy::instance().release(ptr);
}  

// Date.h
class Date {
public: // public interface:
    Date(int day, int month, int year);

     // non-modifying functions for examining the Date:
     int day() const;
     int month() const;
     int year() const;
     const char* toString() const; // C-style string representation
     bool compare(const Date& other) const;

private:
     int _day, _month, _year; // representation
     char _strRepresentation[11]; // C-style string representation
};

// Date.cpp
Date::Date(int day, int month, int year) 
    : _day{day}, _month{month}, _year{year} {
	// build C-style string representation
    std::stringstream stream;
    stream.rdbuf()->pubsetbuf(_strRepresentation, sizeof(_strRepresentation));
    stream << _day << "/" << _month << "/" << year << (char)0;
}

inline int Date::day() const {
     return _day;
}
 
inline int Date::month() const {
     return _month;
}
 
inline int Date::year() const {
     return _year;
}
 
inline const char* Date::toString() const { 
    return _strRepresentation;
}

inline bool Date::compare(const Date& other) const {
    return _day==other._day && _month==other._month && _year==other._year;
}

// Lab5TestSuite.h
class Lab5TestSuite {
    
public:
    Lab5TestSuite();

    int runTests();
    
private:
    typedef void (Lab5TestSuite::*Test)() const;
    
    std::vector<std::pair<const char*, Test>> tests;
    
    void registerTest(const char* description, Test fnTest);
    
    void assert(bool expression, const char* context, const char* message) const;
    
    // Actual tests declarations
    void testCtorWithName() const;

    void testCtorWithNameAndDate() const; 
    
    void testCopyCtor() const;
    
    void testStudentRepresentation() const;  
    
    void testNumberOfInstances() const;
    
    void fnWithReferenceArgument(const Student&) const {};
    
    void fnWithTypeArgument(Student) const {};
} ;


// Lab5TestSuite.cpp
Lab5TestSuite::Lab5TestSuite() {
    registerTest("ctor with name", &Lab5TestSuite::testCtorWithName);
    registerTest("ctor with name and date", &Lab5TestSuite::testCtorWithNameAndDate); 
    registerTest("copy ctor", &Lab5TestSuite::testCopyCtor);
    registerTest("Student representation", &Lab5TestSuite::testStudentRepresentation);
    registerTest("number of instances", &Lab5TestSuite::testNumberOfInstances);
}

int Lab5TestSuite::runTests() {
    int failedTests = std::accumulate(tests.begin(), tests.end(), 0, [this](int counter, std::pair<const char*, Test> entry) { 
        std::cout << "Running test " << entry.first << "...\n";
        const int before = FreeStoreProxy::instance().getDanglingPointersCount();
        (this->*entry.second)();
        int after = FreeStoreProxy::instance().getDanglingPointersCount() - before;
        if (after!=0) {
            std::cout << "Assertion failed: Memory leak in test [" << entry.first << "]\n";
        }
        return counter + (after!=0);
    });
    
    std::cout << "Tests run: " << tests.size() << ", Passed: " << (tests.size()-failedTests) << ", Failed: " << failedTests << std::endl; 
    return 0;
}
    
inline void Lab5TestSuite::assert(bool expression, const char* context, const char* message) const {
    if (!expression) {
        std::cout << "Assertion failed in [" << context << "]: " << message << std::endl;
    }
}

inline void Lab5TestSuite::registerTest(const char* description, Test fnTest) {
    tests.push_back(std::pair<const char*, Test>{description, fnTest});
}

void Lab5TestSuite::testCtorWithName() const {
    Student john{"John Doe"};
    assert(100==john.id(), "testCtorWithName", "internal representation error");
    assert(0==strcmp("John Doe", john.name()), "testCtorWithName", "internal representation error");
    assert(Date{1, 1, 1970}.compare(john.dob()), "testCtorWithName", "internal representation error");
    
    assert(1==Student::numberOfInstance(), "testCtorWithName", "wrong number of total instances");
}

void Lab5TestSuite::testCtorWithNameAndDate() const {
    const Date releaseDate{27, 8, 1974};
    Student mary{"Mary Poppins", releaseDate};
    assert(101==mary.id(), "testCtorWithNameAndDate", "internal representation error");
    assert(0==strcmp("Mary Poppins", mary.name()), "testCtorWithNameAndDate", "internal representation error");
    assert(releaseDate.compare(mary.dob()), "testCtorWithNameAndDate", "internal representation error");
    
    assert(2==Student::numberOfInstance(), "testCtorWithName", "wrong number of total instances");
}

void Lab5TestSuite::testCopyCtor() const {
    const Date birthDate{23, 6, 1912};
    Student alan{"Alan Turing", birthDate};
    assert(102==alan.id(), "testCopyCtor", "internal representation error");
    assert(0==strcmp("Alan Turing", alan.name()), "testCopyCtor", "internal representation error");
    assert(birthDate.compare(alan.dob()), "testCopyCtor", "internal representation error");
    
    Student copyOfAlan{alan};
    assert(102==copyOfAlan.id(), "testCopyCtor", "internal representation error");
    assert(0==strcmp("Alan Turing", copyOfAlan.name()), "testCopyCtor", "internal representation error");
    assert(birthDate.compare(copyOfAlan.dob()), "testCopyCtor", "internal representation error");
    
    copyOfAlan.setName("Alan Turing (2)");
    copyOfAlan.setDob(Date{7, 6, 1954});
    assert(102==alan.id(), "testCopyCtor", "internal representation error");
    assert(0==strcmp("Alan Turing", alan.name()), "testCopyCtor", "internal representation error");
    assert(birthDate.compare(alan.dob()), "testCopyCtor", "internal representation error");
    
    assert(102==copyOfAlan.id(), "testCopyCtor", "internal representation error");
    assert(0==strcmp("Alan Turing (2)", copyOfAlan.name()), "testCopyCtor", "internal representation error");
    assert(Date{7, 6, 1954}.compare(copyOfAlan.dob()), "testCopyCtor", "internal representation error");
    
    assert(4==Student::numberOfInstance(), "testCtorWithName", "wrong number of total instances");
}

void Lab5TestSuite::testStudentRepresentation() const {
    const Date birthDate{24, 6, 1987};
    Student lionel{"Lionel Messi", birthDate};
    assert(103==lionel.id(), "testStringRepresentation", "internal representation error");
    assert(0==strcmp("Lionel Messi", lionel.name()), "testStringRepresentation", "internal representation error");
    assert(birthDate.compare(lionel.dob()), "testStringRepresentation", "internal representation error");
    assert(0==strcmp("{id: 103, name: Lionel Messi, dob: 24/6/1987}", lionel.toString()), "testStringRepresentation", "string representation error");
    
    assert(5==Student::numberOfInstance(), "testStringRepresentation", "wrong number of total instances");
}

void Lab5TestSuite::testNumberOfInstances() const {
    const Date birthDate{7, 2, 1982};
    Student delia{"Delia Matache", birthDate};
    assert(104==delia.id(), "testNumberOfInstances", "internal representation error");
    assert(0==strcmp("Delia Matache", delia.name()), "testNumberOfInstances", "internal representation error");
    assert(birthDate.compare(delia.dob()), "testNumberOfInstances", "internal representation error");
    
    assert(6==Student::numberOfInstance(), "testNumberOfInstances", "wrong number of total instances");
    
    fnWithReferenceArgument(delia);
    assert(6==Student::numberOfInstance(), "testNumberOfInstances", "wrong number of total instances");

    fnWithTypeArgument(delia);
    assert(7==Student::numberOfInstance(), "testNumberOfInstances", "wrong number of total instances");

    Student flickrCoFounder{"Daniel Stewart Butterfield"};
    assert(105==flickrCoFounder.id(), "testNumberOfInstances", "internal representation error");
    assert(8==Student::numberOfInstance(), "testNumberOfInstances", "wrong number of total instances");
}

// main.cpp
int main(void) {
    return Lab5TestSuite{}.runTests();
}

