#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>

using namespace std;

class FreeStoreProxy {
public: 
    FreeStoreProxy();
    
    static FreeStoreProxy& instance();
    
    void* allocate(size_t size);
    
    void release(void* ptr) ;
    
    int getDanglingPointersCount() const;
    
    void freeAll();

private:
    static const int CAPACITY = 1024;
    static FreeStoreProxy _instance;
    void* entries[CAPACITY];
    void* const* position() const;
} ;

FreeStoreProxy FreeStoreProxy::_instance;

inline void* const* FreeStoreProxy::position() const {
    return std::find(entries, entries+CAPACITY, nullptr);
}
    
FreeStoreProxy::FreeStoreProxy() {
    //std::cout << "FreeStoreProxy::FreeStoreProxy" << std::endl;
    std::fill_n(entries, CAPACITY, nullptr); 
}

inline FreeStoreProxy& FreeStoreProxy::instance() {
    return _instance;
}   

void* FreeStoreProxy::allocate(size_t size) {
    void** pos = (void **)position();
    if (pos!=entries+CAPACITY) {
        void* ptr = malloc(size);
        *pos = ptr;
        //std::cout << "new " << size <<  " bytes @" << ptr << " cached @[" << (pos-entries) << "]" << std::endl;
        return ptr;
    }
    return nullptr;
}

void FreeStoreProxy::release(void* ptr) {
    for(int i=0; i<CAPACITY; i++) {
        if (entries[i]==ptr) {
            //std::cout << "delete @" << ptr << std::endl;
            free(ptr);
            entries[i] = nullptr;
            break;
        }
    }
}

int FreeStoreProxy::getDanglingPointersCount() const {
    // int counter = 0;
    // std::for_each(entries, entries+CAPACITY, [&](void* p) { counter += (p!=nullptr); });
    // return counter;
    //return std::accumulate(entries, entries+CAPACITY, 0, [](int a, void* p) { return a + (p!=nullptr); });
    return CAPACITY - std::count(entries, entries+CAPACITY, nullptr);
}

void FreeStoreProxy::freeAll() {
    for(int i=0; i<CAPACITY; i++) {
        if (entries[i]!=nullptr) {
            //std::cout << "delete @" << entries[i] << std::endl;
            free(entries[i]);
            entries[i] = nullptr;
        }
    }
}
        
void * operator new(size_t size) {
    return FreeStoreProxy::instance().allocate(size);
}

void operator delete(void *ptr) noexcept {
    FreeStoreProxy::instance().release(ptr);
}  


enum Gender { MALE, FEMALE };

enum Genre  { ROCK, POP, COUNTRY, JAZZ };

// Lab9TestSuite.h
class Lab9TestSuite {
    
public:
    Lab9TestSuite();

    int runTests();
    
private:
    typedef void (Lab9TestSuite::*Test)() const;
    
    std::vector<std::pair<const char*, Test>> tests;
    
    void registerTest(const char* description, Test fnTest);
    
    void assert(bool expression, const char* context, const char* message) const;
    
    // Actual tests declarations
    void testSinger() const;

    void testBand() const;
    
    void testBandMembers() const;
    
    void testPolymorphism() const;
} ; 

// Lab9TestSuite.cpp
Lab9TestSuite::Lab9TestSuite() {
    registerTest("singer", &Lab9TestSuite::testSinger);
    registerTest("band", &Lab9TestSuite::testBand);
    registerTest("band mmbers", &Lab9TestSuite::testBandMembers);
    registerTest("polymorphism", &Lab9TestSuite::testPolymorphism);
}

int Lab9TestSuite::runTests() {
    int failedTests = std::accumulate(tests.begin(), tests.end(), 0, [this](int counter, std::pair<const char*, Test> entry) { 
        std::cout << "Running test " << entry.first << "...\n";
        const int before = FreeStoreProxy::instance().getDanglingPointersCount();
        (this->*entry.second)();
        int after = FreeStoreProxy::instance().getDanglingPointersCount() - before;
        if (after!=0) {
            std::cout << "Assertion failed: Memory leak in test [" << entry.first << "]\n";
        }
        return counter + (after!=0);
    });
    
    std::cout << "Tests run: " << tests.size() << ", Passed: " << (tests.size()-failedTests) << ", Failed: " << failedTests << std::endl; 
    return 0;
}
    
inline void Lab9TestSuite::assert(bool expression, const char* context, const char* message) const {
    if (!expression) {
        std::cout << "Assertion failed in [" << context << "]: " << message << std::endl;
    }
}

inline void Lab9TestSuite::registerTest(const char* description, Test fnTest) {
    tests.push_back(std::pair<const char*, Test>{description, fnTest});
}

void Lab9TestSuite::testSinger() const {
    Singer johnBonJovi("Jon Bon Jovi", "John Francis Bongiovi Jr.", MALE, ROCK);
    assert(johnBonJovi.name()=="Jon Bon Jovi", "testSinger", "name mismatch");
    assert(johnBonJovi.realName()=="John Francis Bongiovi Jr.", "testSinger", "real name mismatch");
    assert(johnBonJovi.gender()==MALE, "testSinger", "gender mismatch");
    assert(johnBonJovi.genre()==ROCK, "testSinger", "genre mismatch");
}

void Lab9TestSuite::testBand() const {
    Band BonJovi("Bon Jovi", ROCK);
    assert(BonJovi.name()=="Bon Jovi", "testSinger", "name mismatch");
    assert(BonJovi.genre()==ROCK, "testSinger", "genre mismatch");
    
    Band MD5("Miles Davis Quintet", JAZZ);
    assert(MD5.name()=="Miles Davis Quintet", "testSinger", "name mismatch");
    assert(MD5.genre()==JAZZ, "testSinger", "genre mismatch");
}

void Lab9TestSuite::testBandMembers() const {
    Singer smiley("Smiley", "Andrei Tiberiu Maria", MALE, POP);
    assert(smiley.name()=="Smiley", "testSinger", "name mismatch");
    assert(smiley.realName()=="Andrei Tiberiu Maria", "testSinger", "real name mismatch");
    assert(smiley.gender()==MALE, "testSinger", "gender mismatch");
    
    Singer crbl("CRBL", "Eduard Mihail Andreianu ", MALE, POP);
    
    Band Simplu("SIMPLU", POP);
    assert(Simplu.name()=="SIMPLU", "testSinger", "name mismatch");
    assert(Simplu.genre()==POP, "testSinger", "genre mismatch");
    
    Simplu.addMember(smiley);
    Simplu.addMember(crbl);
    
    std::cout << "Simplu members: " << Simplu.members() << std::endl;
}

void Lab9TestSuite::testPolymorphism() const {
    MusicPerformer* p = new Singer("Rihanna", "Robyn Rihanna Fenty", FEMALE, POP);
    std::cout << p->toString() << std::endl;
    delete p;
    
    p = new Band("U2", ROCK);
    std::cout << p->toString() << std::endl;    
    delete p;
}


int main() {
    return Lab9TestSuite{}.runTests();
}
